#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  shiftcrypt.py
#  
#  Copyright 2018  <@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from sources.utils import leggifasta
from sources.agmata_source import agmata
import warnings
warnings.filterwarnings("ignore")
import parser
import argparse
from sources.agmata_source import agmata
def run_agmata(args):
	args=args[1:]
	pa = argparse.ArgumentParser()
	
	
	
	pa.add_argument('-i', '--infile',
						help='the input FASTA file',
						)
	pa.add_argument('-o', '--outfile',
						help='output file',
						default=None)

	results = pa.parse_args(args)
	model=agmata(verbose=-1)
	
	try:
		seqs=leggifasta(results.infile)
	except:
		print 'error in the parsing of the file. Please double check the format. If everyhting is correct, please report the bug to gorlando@vub.be'
		return
	model.load()
	try:
		model.load()
	except:
		print 'error loading the model. Please double check you installed all the dependencies. If everyhting is correct, please report the bug to gorlando@vub.be'
		return
	out={}
	print 'running prediction'
	for prot in seqs.keys():
		out[prot]=model.predict(seqs[prot])
	try:
		out={}
		
		for prot in seqs.keys():
			out[prot]=model.predict(seqs[prot])
			
	except:
		print 'error prediction. Please double check you installed all the dependencies. If everyhting is correct, please report the bug to @gmail.com'
		return
	thr=0.015563146
	if results.outfile!=None:
		f=open(results.outfile,'w')
		f.write('#AA AgmataScore BinaryPrediction\n')
		for prot in out.keys():
			f.write('#'+prot+'\n')
			for i in range(len(out[prot])):
				if out[prot][i]>thr:
					p=1
				else:
					p=0
				f.write(seqs[prot][i]+' '+str(out[prot][i])+' '+str(p)+'\n')
		f.close()
		#print out
	else:
		for prot in out.keys():
			
			print('#'+prot+'\n')
			print('#AA AgmataScore BinaryPrediction\n')
			for i in range(len(out[prot])):
				if out[prot][i]>thr:
					p=1
				else:
					p=0
				print(seqs[prot][i]+' '+str(out[prot][i])+' '+str(p)+'\n')
	print '\nDONE'
			
def main(args):
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(run_agmata(sys.argv))
