#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#TYPE='dyna dynass_grad'
#TYPE='prop'
prop_sw=35
from EF.EFoldMine import EarlyFoldPredictor
from dynamine.dynamineStandalone import DynaMine
from plosone_indices import plosone_dict,plosone_dict_balibase,plosone_dict_disorder
from propensity import propertyTable as pr
from propensity import aminoacids,compressed,hidrophobicity1,hidrophob_rose,hidrophob_kyte,spritz_indices
import numpy,pickle

DIR_ALN='alignments/nmr.fastaAlignDir/'
ss_grad=',dyna_coil_grad,dyna_helix_grad,dyna_sheet_grad,'
ss=',dyna_coil,dyna_helix,dyna_sheet,'
#TYPE='hidrophob1'#pssm'#,compressed'+ss#aa'#,dyna_back,dyna_side'
#TYPE_gap=ss#'pssm'#'aa,dyna_back,dyna_side'
COMP='vicini'
#from Bio.SubsMat import MatrixInfo
#blosum = MatrixInfo.blosum62
import numpy as np
import string,random,os
### objects ###
ef_obj=EarlyFoldPredictor()
amino_index={'A': 1, 'C':2, 'B': -1, 'E': 3, 'D': 4, 'G': 5, 'F': 6, 'I': 7, 'H': 8, 'K': 9, 'M': 10, 'L': 11, 'N': 12, 'Q': 13, 'P': 14, 'S':15, 'R': 16, 'U':-1, 'T': 17, 'W': 18, 'V': 0, 'Y': 19, 'X': -1, 'Z': -1}
### dynamine objects
D_coil = DynaMine(types='coil')
D_back = DynaMine(types='backbone')
D_helix = DynaMine(types='helix')
D_side = DynaMine(types='sidechain')
D_sheet = DynaMine(types='sheet')
class ChargeAssymetry:

	def setSequence(self,sequence):

		self.sequence = sequence

	def calculateChargeAssymetry(self,window=7,negRes='DE',posRes='RK'):

		halfWindow = window / 2

		# Make a fake sequence to run code on, also set a positive charge
		# at N term and a negative at C term
		checkSeq = (halfWindow - 1) * '-' + "R" + self.sequence + "D" + (halfWindow - 1) * '-' 
		tot=[]
		vet=[]
		for i in range(halfWindow,len(self.sequence)+halfWindow):
			seqFrag = checkSeq[i-halfWindow:i+halfWindow+1]

			posResFrac = sum([seqFrag.count(pRes) for pRes in posRes]) * 1.0 / window
			negResFrac = sum([seqFrag.count(nRes) for nRes in negRes]) * 1.0 / window
			if posResFrac + negResFrac==0:
				sigma=0.0
			else:
				
				sigma = (posResFrac - negResFrac) ** 2.0 / (posResFrac + negResFrac)
			tot+=[sigma]
		return tot

class psipred:
	def __init__(self,single_sequence=True,psipred_single_binary=os.getcwd()+'/vector_builder/psipred/runpsipred_single',tmpfolder=os.getcwd()+'/vector_builder/psipred/tmp/'):
		if single_sequence:
			self.binary=psipred_single_binary
		else:
			raise(NotImplementedError)
		self.tmpfold=tmpfolder
	def predict(self,seq):
		name=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(7))
		f=open(self.tmpfold+name+'.fasta','w')
		pred=[]
		f.write('>seq\n'+seq)
		f.close()
		os.system(self.binary+' '+self.tmpfold+name+'.fasta > /dev/null')
		f=open(name+'.ss2').readlines()
		for i in f[2:]:
			a=i.strip().split()
			if len(a)==0:
				continue
			else:
				pred+=[[float(a[3]),float(a[4]),float(a[5])]]
		os.system('rm *.horiz')
		os.system('rm *.ss')
		os.system('rm *.ss2')
		return pred
def sliding_w(vet,intorno=3):#[[1,2],[1,3],[1,2]]
	vetfin=[]
	
	
	for i in range(len(vet)):
		if i-intorno<0:
			iniz=0
		else:
			iniz=i-intorno
		if i+intorno+1>len(vet):
			fin=len(vet)
		else:
			fin=i+intorno+1
		a=vet[iniz:fin]
		c=[]
		for fea in range(len(a[0])):
			s=0.0
			for p in range(len(a)):
				if type(a[p][fea])!=str:
					s+=a[p][fea]
			c+=[s/len(a)]
		vetfin+=[c]
	return vetfin	

def norm_prop(propertyTable):
	normal={}
	for i in propertyTable.keys():
		normal[i]=[]
	for j in range(len(propertyTable['A'])): #for each feature
		maxim=-float('inf')
		minim=float('inf')
		for i in propertyTable.keys():
			if propertyTable[i][j]>maxim:
				maxim=propertyTable[i][j]
			if propertyTable[i][j]<minim:
				minim=propertyTable[i][j]
		for i in propertyTable.keys():
			normal[i]+=[(propertyTable[i][j]-minim)/(maxim-minim)]
	return normal
def compare(v1,v2):
	vf=[]
	#print v1,v2
	if COMP=='diff':
		
		if '-' in v1:
			vf=[]
			for i in v2:
				if type(i)!=str:
					vf+=[i]
			return vf
		elif '-' in v2:
			vf=[]
			for i in v1:
				if type(i)!=str:
					vf+=[i]
			return vf
		else:
		
			for i in range(len(v1)):
				
				if type(v1[i])!=str:
					vf+=[abs(v1[i]-v2[i])]
				
				else:
					if blosum.has_key((v1[i].upper(),v2[i].upper())):
						
						vf+=[blosum[(v1[i].upper(),v2[i].upper())]]
					else:
						
						vf+=[blosum[(v2[i].upper(),v1[i].upper())]]
	if COMP=='vicini':
		if '-' in v1:
			vf=[]
			for i in v2:
				if type(i)!=str:
					vf+=[i]
			return vf
		elif '-' in v2:
			vf=[]
			for i in v1:
				if type(i)!=str:
					vf+=[i]
			return vf
		else:
		
			for i in range(len(v1)):
				
				if type(v1[i])!=str:
					vf+=[v1[i]-v2[i]]
				
				else:
					if blosum.has_key((v1[i].upper(),v2[i].upper())):
						
						vf+=[blosum[(v1[i].upper(),v2[i].upper())]]
					else:
						
						vf+=[blosum[(v2[i].upper(),v1[i].upper())]]
	
	return vf
	
def vettore(aln_seq,position,typ,sw,nomeseq=None):#i take the aligned seq and the position in order to build the vector with sliding window values too
	
	fin=[]
	posEffettiva=0
	propertyTable=norm_prop(pr)
	#propertyTable=pr
	i=0
	while i<position:
		if aln_seq[i]!='-' and aln_seq[i]!='.':
		
			posEffettiva+=1
		i+=1
	seq=aln_seq.replace('-','').replace('.','')
	if 'amino_blos' in typ.split(','):
		for i in range(-sw2,sw2+1):
			if posEffettiva+i<0:
				fin+=['A']
				
			elif posEffettiva+i>=len(seq):
				fin+=['A']
			else:
				fin+=[seq[i+posEffettiva]]
		
	if 'prop' in typ.split(','):
		nfeatures=len(propertyTable['A'])
		for i in range(-sw,sw+1):
			if posEffettiva+i<0:
				fin+=[0]*nfeatures
			elif posEffettiva+i>=len(seq):
				fin+=[0]*nfeatures
			else:
				fin+=propertyTable[seq[posEffettiva+i].upper()]

	if 'aa' in typ.split(','):
		nfeatures=20
		for i in range(-sw,sw+1):
			if posEffettiva+i<0:
				fin+=[0]*nfeatures
			elif posEffettiva+i>=len(seq):
				fin+=[0]*nfeatures
			else:
				fin+=aminoacids[seq[posEffettiva+i]]
	if 'compressed' in typ.split(','):
		nfeatures=6
		for i in range(-sw,sw+1):
			if posEffettiva+i<0:
				fin+=[0]*nfeatures
			elif posEffettiva+i>=len(seq):
				fin+=[0]*nfeatures
			else:
				fin+=compressed[seq[posEffettiva+i]]
	if 'plosone_ind' in typ.split(','):
		nfeatures=16
		for i in range(-sw,sw+1):
			if posEffettiva+i<0:
				fin+=[0]*nfeatures
			elif posEffettiva+i>=len(seq):
				fin+=[0]*nfeatures
			else:
				fin+=plosone_dict[seq[posEffettiva+i]]
	if 'plosone_ind_disorder' in typ.split(','):
		nfeatures=22
		for i in range(-sw,sw+1):
			if posEffettiva+i<0:
				fin+=[0]*nfeatures
			elif posEffettiva+i>=len(seq):
				fin+=[0]*nfeatures
			else:
				fin+=plosone_dict_disorder[seq[posEffettiva+i]]
	if 'plosone_ind_balibase' in typ.split(','):
		nfeatures=len(plosone_dict_balibase['A'])
		intorno=2
		i=posEffettiva
		if i-intorno<0:
			iniz=0
		else:
			iniz=i-intorno
		if i+intorno+1>len(seq):
			final=len(seq)
		else:
			final=i+intorno+1
		c=[]
		for fea in range(nfeatures):
			s=0.0
			for p in seq[iniz:final]:
				s+=plosone_dict_balibase[p][fea]
			c+=[s/len(seq[iniz:final])]
		fin+=c
	if 'hidrophob1' in typ.split(','):
		nfeatures=1
		for i in range(-sw,sw+1):
			if posEffettiva+i<0:
				fin+=[1]*nfeatures
			elif posEffettiva+i>=len(seq):
				fin+=[1]*nfeatures
			else:
				fin+=[hidrophobicity1[seq[posEffettiva+i]]]
	if 'hidrophob_rose' in typ.split(','):
		nfeatures=1
		for i in range(-sw,sw+1):
			if posEffettiva+i<0:
				fin+=[0.7]*nfeatures
			elif posEffettiva+i>=len(seq):
				fin+=[0.7]*nfeatures
			else:
				fin+=[hidrophob_rose[seq[posEffettiva+i]]]
	if 'hidrophob_kyte' in typ.split(','):
		nfeatures=1
		for i in range(-sw,sw+1):
			if posEffettiva+i<0:
				fin+=[1]*nfeatures
			elif posEffettiva+i>=len(seq):
				fin+=[1]*nfeatures
			else:
				fin+=[hidrophob_rose[seq[posEffettiva+i]]]
	return fin
def build_vector(seq,TYPE=None,sw=None,nomeseq=None):
	vet=[]
	last=0
	seq_nogap=seq.replace('-','')
	for i in range(len(seq)):
		vet+=[[]]
	nfeatures=0	
	for curr_fea in TYPE.split(','):
		if 'dyna_coil' == curr_fea:
			#D_coil = DynaMine(types='coil')
			v_dyna=D_coil.predictSeqs(seq_nogap)
			effett=0
			#v_dyna=numpy.gradient(numpy.array(v_dyna))
			
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]
						else:
							vet[i]+=[v_dyna[effett+s]]
					effett+=1
					last=i
		elif 'dyna_coil_grad' == curr_fea:
			#D = DynaMine()
			v_dyna=D_coil.predictSeqs(seq_nogap)
			effett=0
			v_dyna=numpy.gradient(numpy.array(v_dyna))
			
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]
						else:
							vet[i]+=[v_dyna[effett+s]]
					effett+=1
					last=i	
		elif 'dyna_helix_grad' == curr_fea:
			#D = DynaMine()
			v_dyna=D_helix.predictSeqs(seq_nogap)
			effett=0
			v_dyna=numpy.gradient(numpy.array(v_dyna))
			
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]
						else:
							vet[i]+=[v_dyna[effett+s]]
					effett+=1
					last=i	
		elif 'dyna_helix' == curr_fea:
			#D = DynaMine(types='helix')
			v_dyna=D_helix.predictSeqs(seq_nogap)
			effett=0
			#v_dyna=numpy.gradient(numpy.array(v_dyna))
			
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]
						else:
							vet[i]+=[v_dyna[effett+s]]
					effett+=1
					last=i	
		elif 'dyna_sheet' == curr_fea:
			#D = DynaMine(types='sheet')
			v_dyna=D_sheet.predictSeqs(seq_nogap)
			effett=0
			#v_dyna=numpy.gradient(numpy.array(v_dyna))
			
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]
						else:
							vet[i]+=[v_dyna[effett+s]]
					effett+=1
					last=i	
		elif 'dyna_sheet_grad' == curr_fea:
			#D = DynaMine()
			v_dyna=D_sheet.predictSeqs(seq_nogap)
			effett=0
			v_dyna=numpy.gradient(numpy.array(v_dyna))
			
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]
						else:
							vet[i]+=[v_dyna[effett+s]]
					effett+=1
					last=i
		elif 'ChargeAssymetry' == curr_fea:
			D = ChargeAssymetry()
			D.setSequence(seq_nogap)
			effett=0
			v_dyna=D.calculateChargeAssymetry(window=7,negRes='DE',posRes='RK')
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]
						else:
							vet[i]+=[v_dyna[effett+s]]
					effett+=1
					last=i
		elif 'dyna_side_grad' == curr_fea:
			#D = DynaMine()
			v_dyna=D_side.predictSeqs(seq_nogap)
			effett=0
			v_dyna=numpy.gradient(numpy.array(v_dyna))
			
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]
						else:
							vet[i]+=[v_dyna[effett+s]]
					effett+=1
					last=i	
		elif 'dyna_side' == curr_fea:
			#D = DynaMine(types='sidechain')
			v_dyna=D_side.predictSeqs(seq_nogap)
			effett=0
			#v_dyna=numpy.gradient(numpy.array(v_dyna))
			
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]
						else:
							vet[i]+=[v_dyna[effett+s]]
					effett+=1
					last=i	
		elif 'dyna_back' == curr_fea:
			#D = DynaMine(types='backbone')
			v_dyna=D_back.predictSeqs(seq_nogap)
			effett=0
			#v_dyna=numpy.gradient(numpy.array(v_dyna))
			
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]
						else:
							vet[i]+=[v_dyna[effett+s]]
					effett+=1
					last=i	
		elif 'dyna_coil_grad' == curr_fea:
			#D = DynaMine()
			v_dyna=D_coil.predictSeqs(seq_nogap)
			effett=0
			v_dyna=numpy.gradient(numpy.array(v_dyna))
			
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]
						else:
							vet[i]+=[v_dyna[effett+s]]
					effett+=1
					last=i	
		elif 'dyna_back_grad' == curr_fea:
			#D = DynaMine()
			v_dyna=D_back.runDynaMineSeqs(seq_nogap,'2',dataType='backbone')
			effett=0
			v_dyna=numpy.gradient(numpy.array(v_dyna))
			
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]
						else:
							vet[i]+=[v_dyna[effett+s]]
					effett+=1
					last=i	
		elif 'ef' == curr_fea:
			
			v_dyna=ef_obj.predictEarlyFolding(seq_nogap)
			effett=0
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]
						else:
							vet[i]+=[v_dyna[effett+s]]
					effett+=1
					last=i	
		elif 'pssm' == curr_fea:
			#suffisso='_jh_eval0.0001_.hmmer'
			suffisso='_iter1_eval0.0001_.hh'
			assert nomeseq!=None

			aln=open(DIR_ALN+nomeseq+suffisso).readlines()
			for i in range(len(aln)):
				aln[i]=list(aln[i].strip())
			ami=[0.0]*20
			pssm=[]
			aln=numpy.array(aln).transpose()
			for i in range(len(aln)):
				unique, counts = np.unique(aln[i], return_counts=True)
				cc= np.asarray((unique, counts)).T
				ami=[0.0]*20
				for j in cc:
					if j[0]!='-':
						l=amino_index[j[0]]
						if l!=-1:
							#print j
							ami[l]=int(j[1])
				pssm+=[ami]
			for i in range(len(pssm)):
				su=float(sum(pssm[i]))
				for j in range(len(pssm[i])):
					pssm[i][j]=pssm[i][j]/su	
			effett=0	
			for i in range(len(vet)):
				if seq[i]!='-':
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]*20
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]*20
						else:
							vet[i]+=pssm[effett+s]
					effett+=1
					last=i
		elif 'entropy' == curr_fea:
			import math
		
			assert nomeseq!=None
			#suffisso='_jh_eval0.0001_.hmmer'
			suffisso='_iter1_eval0.0001_.hh'
			f=open(DIR_ALN+nomeseq+suffisso)
			ami=[0.0]*20
			pssm=[]
			for i in seq.replace('-',''):
				pssm+=[ami[:]]
			#print numpy.array(pssm).shape
			num=[0.0]*len(seq)
			
			aln1=f.readlines()
			aln1[0]=aln1[0].replace('O-ID\n','')
			
			aln=[]
			for i in range(len(aln1)):
				aln+=[list(aln1[i][0:len(aln1[0])].strip('\n'))]
			aln=numpy.array(aln)
			ef=0
			
			for i in range(len(aln[0])):
				if aln[0][i]=='-':
					continue
				
				for j in aln[:,i]:
					if j!='-':
						if amino_index.has_key(j):
							
							pssm[ef][amino_index[j]]+=1
						#print pssm[ef][amino_index[j]]
							num[ef]+=1
				ef+=1
			entropy=[]
			for i in range(len(pssm)):
				for j in range(len(pssm[i])):
					pssm[i][j]=float(pssm[i][j])/float(num[i])
			for i in pssm:
				en=0
				for j in i:
					if j>0:
						
						en+=j*math.log(j,2)
					else:
						j=0.000000000000000001
						en+=j*math.log(j,2)
				#print en		
				entropy+=[-en]
			effett=0	
			for i in range(len(vet)):
				if seq[i]!='-':
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]*1
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]*1
						else:
							vet[i]+=[entropy[effett+s]]
					effett+=1
					last=i
		elif 'plosone_ind' == curr_fea:
			effett=0
			for i in range(len(vet)):
				if seq[i]!='-':
					for s in range(-sw,sw+1):
						if effett+i<0:
							vet[i]+=[0]*16
						elif effett+i>=len(seq):
							vet[i]+=[0]*16
						else:
							vet[i]+=plosone_dict[seq_nogap[effett+s]]

					effett+=1
					last=i
		elif 'hidrophob_kyte' == curr_fea:
			effett=0
			for i in range(len(vet)):
				if seq[i]!='-':
					for s in range(-sw,sw+1):
						if effett+i<0:
							vet[i]+=[0]
						elif effett+i>=len(seq):
							vet[i]+=[0]
						else:
							vet[i]+=[hidrophob_kyte[seq_nogap[effett+s]]]
					assert len(vet[i])!=0
					effett+=1
					last=i
		
		elif 'plosone_ind' == curr_fea:
			effett=0
			for i in range(len(vet)):
				if seq[i]!='-':
					for s in range(-sw,sw+1):
						if effett+i<0:
							vet[i]+=[0]*16
						elif effett+i>=len(seq):
							vet[i]+=[0]*16
						else:
							vet[i]+=plosone_dict[seq_nogap[effett+s]]

					effett+=1
					last=i 	
		elif 'plosone_ind_disorder' == curr_fea:
			effett=0
			for i in range(len(vet)):
				if seq[i]!='-':
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]*len(plosone_dict_disorder['A'])
						elif effett+s>=len(seq):
							vet[i]+=[0]*len(plosone_dict_disorder['A'])
						else:
							vet[i]+=plosone_dict_disorder[seq_nogap[effett+s]]

					effett+=1
					last=i 	
		elif 'plosone_ind_disorder_sliding' == curr_fea:
			effett=0
			v=[]
			for i in range(len(vet)):
				if seq[i]!='-':

					if effett+i<0:
						v+=[[0]*len(plosone_dict_disorder['A'])]
					elif effett+i>=len(seq):
						v+=[[0]*len(plosone_dict_disorder['A'])]
					else:
						v+=[plosone_dict_disorder[seq_nogap[effett]]]

					effett+=1
					last=i
			v=sliding_w(v)
			effett=0
			for i in range(len(vet)):
				if seq[i]!='-':
					vet[i]+=v[effett]
					#print len(seq_nogap),effet,len(v)
					effett+=1
					last=i
		elif 'spritz_indices' == curr_fea:
			effett=0
			for i in range(len(vet)):
				if seq[i]!='-':
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]*len(spritz_indices['A'])
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]*len(spritz_indices['A'])
						else:
							vet[i]+=spritz_indices[seq_nogap[effett+s]]

					effett+=1
					last=i 	
		elif 'compressed' == curr_fea:
			effett=0
			for i in range(len(vet)):
				if seq[i]!='-':
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]*len(compressed['A'])
						elif effett+s>=len(seq):
							vet[i]+=[0]*len(compressed['A'])
						else:
							vet[i]+=compressed[seq_nogap[effett+s]]

					effett+=1
					last=i 	
		elif 'psipred' == curr_fea:
			D = psipred()
			v_dyna=D.predict(seq_nogap)
			effett=0
			#v_dyna=numpy.gradient(numpy.array(v_dyna))
			
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]*3
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]*3
						else:
							vet[i]+=v_dyna[effett+s]
					effett+=1
					last=i
		elif 'pfamscan' == curr_fea:
			
			v_dyna=pfam_diz[nomeseq]
			effett=0
			#v_dyna=numpy.gradient(numpy.array(v_dyna))
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]
						else:
							vet[i]+=[v_dyna[effett+s]]
					effett+=1
					last=i
		elif 'linear_entropy_alignment' == curr_fea:
			#suffisso='_jh_eval0.0001_.hmmer'
			suffisso='_iter1_eval0.0001_.hh'
			assert nomeseq!=None
			ws=2
			aln=open(DIR_ALN+nomeseq+suffisso).readlines()
			entro=[]
			for i in range(len(aln)):
				aln[i]=list(aln[i].strip())
				tmp=[]
				for j in range(len(aln[i])):
					if j-ws<0:
						ini=0
					else:
						ini=j-ws
					if j+ws>len(aln[i]):
						fin=len(aln[i])
					else:
						fin=j+ws
					a=aln[i][ini:fin]
					if not '-' in a:
						unique, counts = np.unique(a, return_counts=True)
						cc= np.asarray((unique, counts)).T
						tmp+=[len(cc)]
					else:
						tmp+=[-1]
				entro+=[tmp]
		
			entro=np.array(entro).transpose()
			#print entro.shape
			profile=[]
			cont=0
			for i in entro:
				
				a = [x for x in i if x != -1]
				profile+=[float(np.mean(a)/min(20.0,ws*2+1))]

			effett=0

			for i in range(len(vet)):
				if seq[i]!='-':
					
					for s in range(-sw,sw+1):
						
						if effett+s<0:
							vet[i]+=[0]*1
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]*1
						else:
							
							vet[i]+=[profile[effett+s]]
					effett+=1
					last=i
				


		else:
			print curr_fea,'IS NOT A FEATURE'
			assert False
	#nfeatures=len(vet[last])+len(vettore(seq_nogap,0,TYPE,sw,nomeseq))
	for i in range(len(seq)):
		if seq[i]=='-' or seq[i]=='.':
			vet[i]+=['-']*nfeatures

	return vet
def fai_vettore_pairwise(s1,s2):
	v1=build_vector(s1,'culo')
	v2=build_vector(s2,'culo')
	fin=[]
	for i in range(len(v1)):
		fin+=[compare(v1[i],v2[i])]
	return fin
def leggifasta(database): #legge un file fasta e lo converte in un dizionario
	f=open(database)
	uniprot=f.readlines()
	f.close()
	dizio={}
	for i in uniprot:
		#c=re.match(">(\w+)",i)  4
	
		if i[0]=='>':
				uniprotid=i.strip('>\n')
				dizio[uniprotid]=''
		else:
			dizio[uniprotid]=dizio[uniprotid]+i.strip('\n')
	return dizio		
if __name__ == '__main__':
	print sliding_w([[0,2],[3,4],[6,6]])#[[1,2],[1,3],[1,2]]
	#TYPE='prop'
	#k = DynaMine()
	#print k.runDynaMineSeqs('RRLMTDRSVGNCIACHEVTEMADAQFPGTVGPS---------------LDGVAARYP-----EAMIRGILVNSKNV----FPETVMPAYYRVEGFNRPGIAFTSKPIEGEIRPLMTAGQIEDVVAYLMTLT'.replace('-',''),'1')[0][40]
	#print  build_vector('NLFAAAAACCCCCCCCAAAAAA','1abo_A')[0]
	s1='AAAAA'#'MSSTQFNKGPSYGLSAEVKNRLLSKYDPQKEAELRTWIEGLTGLSIGPDFQKGLKDGTILCTLMNKLQPGSVPKINRSMQNWHQLENLSNFIKAMVSYGMNPVDLFEANDLFESGNMTQVQVSLLALAGKAKTKGLQSGVDIGVKYSEKQERNFDDATMKAGQCVIGLQMGTNKCASQSGMTAYGTRRHLYDPKNHILPPMDHSTISLQMGTNKCASQVGMTAPGTRRHIYDTKLGTDKCDNSSMSLQMGYTQGANQSGQVFGLGRQIYDPKYCPQGTVADGAPSGTGDCPDPGEVPEYPPYYQEEAGY'
	s2='AAANCFFG-CCCRRREEEDDAAAAAAAAAAAAAADDDDDDC-AACCC'
	a=build_vector(s1,'psipred',0)
	print a
	#import pickle
	#pickle.dump(k,open('ef_res','w'))
	
	print a
	#print a#a#=numpy.array(a)
	s=0.0
	#for i in a:
	#	print i
		
	#print a[0]
	#print fai_vettore_pairwise(s1,s2)
	#print sliding_w('AAACCY')
