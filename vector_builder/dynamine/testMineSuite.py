from predictMineSuite import MineSuitePredictor

if __name__ == '__main__':

  sequences = (('seq1','AKLPEERTGYYWQEENM'),('seq2','VNMGHFDDSERTYIPLKHHGAQWSCCVNM'))


  msp = MineSuitePredictor()
  preds = msp.predictMineSuite(sequences = 'AKLPEERTGYYWQEENM', verbose = False)

  protNames = preds.keys()
  protNames.sort()

  for protName in protNames:
    print protName

    predTypes = preds[protName].keys()
    predTypes.sort()

    for predType in predTypes:
      print predType, preds[protName][predType]
      print
    print

