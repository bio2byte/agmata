#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  utils.py
#  
#  Copyright 2016 Gabriele Orlando <orlando.gabriele89@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import os,marshal
DSSP_BIN='./bin/dssp_bin'
def mkdssp(pdb_file,dssp_out_file='DSSP',folder=False):
	os.system('mkdir '+dssp_out_file)
	if folder:
		for i in os.listdir(pdb_file):
			#print DSSP_BIN+' -i '+pdb_file+'/'+i+' -o '+dssp_out_file+'/'+i.replace('.pdb','')+'.dssp'
			os.system(DSSP_BIN+' -i '+pdb_file+'/'+i+' -o '+dssp_out_file+'/'+i.replace('.pdb','')+'.dssp')
def get_beta(dssp_file,folder=False):
	diz={}
	if folder:
		contsalto=0
		Nparal=0.0
		Nanti=0.0
		tutto=0
		diz_par={}
		diz_anti={}
		Nab={}
		strand=0
		old_label=''
		for j in os.listdir(dssp_file)[:]:
			
			
			seq=''
			sheet_diz={}
			beta={}
			beta_resi=[]
			start=False
			for i in open(dssp_file+j):
				if   '#  RESIDUE AA' in i:
					start=True
					continue
				if not start:
					continue
				#print i
				ss=i[16]
				seq+=i[13]
				index=int(i[0:5])-1
				
				if ss=='E':
					old_ind=index
					
					sheet_label=i[33]
					if sheet_diz.has_key(sheet_label):
						sheet_diz[sheet_label]+=[index]
					else:
						sheet_diz[sheet_label]=[index]
					p1= int(i[25:29])-1
					p2= int(i[29:33])-1
					if p2!=-1:
						beta[index]=[p1,p2]
					else:
						beta[index]=[p1]
			for i in sheet_diz.keys():
				stre=-1
				old=-2
				for q in range(len(sheet_diz[i])):
					if sheet_diz[i][q]-old>2:
						stre+=1
						sheet_diz[i+str(stre)]=[]
					sheet_diz[i+str(stre)]+=[sheet_diz[i][q]]
					old=sheet_diz[i][q]
				del sheet_diz[i]
			###  sheet_diz ### contiene glui strand della proteina!
			beta_reverse={}
			for i in sheet_diz.keys():
				for k in sheet_diz[i]:
					beta_reverse[k]=i
			primo=True
			cont=0
			parallel=[]
			antiparallel=[]
			for i in range(len(seq)):
				for q in range(i+2,len(seq)):
					if Nab.has_key(tuple(sorted([seq[i],seq[q]]))):
						Nab[tuple(sorted([seq[i],seq[q]]))]+=1
					else:
						Nab[tuple(sorted([seq[i],seq[q]]))]=1
			
			##### scrivi qualcosa per discriminare i paralleli dagli antiparalleli (strands interi) ####
			topology={}
			
			for i in sheet_diz.keys():
				for q in sheet_diz.keys():
					if i==q:
						continue
					for res1 in sheet_diz[i]:
						
						for k in beta[res1]:
							if k in sheet_diz[q]:
								if beta .has_key(res1+1) and k+1 in beta[res1+1]:
									
									topology[tuple(sorted([i,q]))]='P'
								elif beta .has_key(res1-1) and k+1 in beta[res1-1]:
									topology[tuple(sorted([i,q]))]='A'
									
			
			for i in topology.keys():
				for k in sheet_diz[i[0]]:
					for l in sheet_diz[i[1]]:
						if topology[i]=='P':
							
							if l in beta[k]:
							
								parallel+=[(k,l)]
						else:
							if l in beta[k]:
								antiparallel+=[(k,l)]
			if parallel!=[] or antiparallel!=[]:
				diz[j.replace('.dssp','')]=(seq.upper().replace('!','A'),parallel,antiparallel)
		#print diz.keys()
		return diz
		'''
		##################################################################à	
		### coefficient replication ###
		for k in parallel:
			a=tuple(sorted([seq[k[0]],seq[k[1]]]))
			if diz_par.has_key(a):
				diz_par[a]+=1
			else:
				diz_par[a]=1.0
		for k in antiparallel:
			a=tuple(sorted([seq[k[0]],seq[k[1]]]))
			if diz_anti.has_key(a):
				diz_anti[a]+=1
			else:
				diz_anti[a]=1.0
		Nparal+=len(parallel)
		Nanti+=len(antiparallel)
	from math import log
	Nabtot=0.0
	#print Nparal,(Nparal+Nanti)
	for k in Nab.keys():
		Nabtot+=Nab[k]
	score= (diz_par[('K','L')]/Nab[('K','L')])/(Nparal/Nabtot)
	print -log(score)
	'''
def readRegions(f):
	def parsePos(pos):
		l = []
		while len(pos) > 0:
			l.append((int(pos.pop(0)),int(pos.pop(0))))
		return l
	ifp = open(f)
	db = {}
	lines = ifp.readlines()
	ifp.close()
	i = 0
	while len(lines) > i:
		name = lines[i].strip()
		i+=1
		seq = lines[i].strip()
		i+=1
		pos = lines[i].strip().split("_")
		assert len(pos) % 2 == 0
		i+=1
		db[name] = [seq, parsePos(pos)]
	dbn={}
	for i in db.keys():
		y=[0]*len(db[i][0])
		for j in db[i][1]:
			for k in range(j[0],j[1]+1):
				y[k]=1
		
		dbn[i[:34]]=(db[i][0],y)
	return dbn
def getScoresSVR(pred, real, threshold,FULL_SCORES=True):
	import math
	import numpy as np
	if len(pred) != len(real):
		raise Exception("ERROR: input vectors have differente len!")
	i = 0
	confusionMatrix = {}
	confusionMatrix["TP"] = confusionMatrix.get("TP", 0)
	confusionMatrix["FP"] = confusionMatrix.get("FP", 0)
	confusionMatrix["FN"] = confusionMatrix.get("FN", 0)
	confusionMatrix["TN"] = confusionMatrix.get("TN", 0)
	while i < len(real):
		if float(pred[i])<=threshold and (int(real[i])==0):
			confusionMatrix["TN"] = confusionMatrix.get("TN", 0) + 1
		if float(pred[i])<=threshold and int(real[i])==1:
			confusionMatrix["FN"] = confusionMatrix.get("FN", 0) + 1
		if float(pred[i])>=threshold and int(real[i])==1:
			confusionMatrix["TP"] = confusionMatrix.get("TP", 0) + 1
		if float(pred[i])>=threshold and int(real[i])==0:
			confusionMatrix["FP"] = confusionMatrix.get("FP", 0) + 1
		i += 1
	#print confusionMatrix
	if FULL_SCORES:
	
		#print "--------------------------------------------"
		#print confusionMatrix["TN"],confusionMatrix["FN"],confusionMatrix["TP"],confusionMatrix["FP"]
		#print "          | SSBOND            | FREE             |"
		#print "predBond  | TP: %d (%2.2f%%)  | FP: %d (%2.2f%%) |" % (confusionMatrix["TP"], (confusionMatrix["TP"]/float(confusionMatrix["TP"]+confusionMatrix["FN"]))*100, confusionMatrix["FP"], (confusionMatrix["FP"]/float(confusionMatrix["FP"]+confusionMatrix["TN"]))*100 )
		#print "predFree  | FN: %d (%2.2f%%)  | TN: %d (%2.2f%%) |" % (confusionMatrix["FN"],(confusionMatrix["FN"]/float(confusionMatrix["TP"]+confusionMatrix["FN"]))*100, confusionMatrix["TN"], (confusionMatrix["TN"]/float(confusionMatrix["FP"]+confusionMatrix["TN"]))*100)
		sen = (confusionMatrix["TP"]/float((confusionMatrix["TP"] + confusionMatrix["FN"])))
		spe = (confusionMatrix["TN"]/float((confusionMatrix["TN"] + confusionMatrix["FP"])))
		acc =  (confusionMatrix["TP"] + confusionMatrix["TN"])/float((sum(confusionMatrix.values())))
		bac = (0.5*((confusionMatrix["TP"]/float((confusionMatrix["TP"] + confusionMatrix["FN"])))+(confusionMatrix["TN"]/float((confusionMatrix["TN"] + confusionMatrix["FP"])))))
		#inf =((confusionMatrix["TP"]/float((confusionMatrix["TP"] + confusionMatrix["FN"])))+(confusionMatrix["TN"]/float((confusionMatrix["TN"] + confusionMatrix["FN"])))-1.0)
		pre =(confusionMatrix["TP"]/float((confusionMatrix["TP"] + confusionMatrix["FP"])))
		mcc =	( ((confusionMatrix["TP"] * confusionMatrix["TN"])-(confusionMatrix["FN"] * confusionMatrix["FP"])) / math.sqrt((confusionMatrix["TP"]+confusionMatrix["FP"])*(confusionMatrix["TP"]+confusionMatrix["FN"])*(confusionMatrix["TN"]+confusionMatrix["FP"])*(confusionMatrix["TN"]+confusionMatrix["FN"])) )  
	
	#print real
	#print pred
	from sklearn.metrics import roc_auc_score
	aucScore = roc_auc_score(real, pred)
	
	#print "AUC = %3.3f" % aucScore
	#print "--------------------------------------------"
	if FULL_SCORES:
		return sen,spe,acc,pre,mcc,aucScore
	else:
		return aucScore
def leggifasta(database): #legge un file fasta e lo converte in un dizionario
		f=open(database)
		uniprot=f.readlines()
		f.close()
		dizio={}
		for i in uniprot:
			#c=re.match(">(\w+)",i)  4
		
			if i[0]=='>':
					if '|'==i[3] and  '|'==i[10]:
						uniprotid=i.strip('>\n').split('|')[1]
					else:
						uniprotid=i.strip('>\n').split(' ')[0]
					dizio[uniprotid]=''
			else:
				dizio[uniprotid]=dizio[uniprotid]+i.strip('\n')
		return dizio
def parse_dynamine(fil='dyna_pred/amyProt_backbone.pred',outfil='marshalled/dyna_back.m'):
	lin=open(fil).readlines()
	lin+=open(fil.replace('amyProt','strands')).readlines()
	#amyProtMissing
	lin+=open(fil.replace('amyProt','amyProtMissing')).readlines()
	print fil.replace('strands','amyProtMissing')
	diz={}
	for i in lin:
		
		if i[0:3]=='***' or i[0:3]=='* P' or i[0:3]=='*  ' or i[0:3]=='* o' or i[0:3]=='* h' or i[0:3]=='* I' or i[0:3]=='* -' or i.strip()=='' or i[0:3]=='* D':
			continue
		elif i[:5]=="* for":
			
			nome=i.replace('* for ','').strip().strip('*').strip()
			print nome
			diz[nome]=[]
		else:
			diz[nome]+=[float(i.strip().split()[1])]
	marshal.dump(diz,open(outfil,'w'))
	return diz
def read_rita2(database): #legge un file fasta e lo converte in un dizionario
		f=open(database)
		uniprot=f.readlines()
		f.close()
		dizio={}
		salto=0
		for i in uniprot:
			#c=re.match(">(\w+)",i)  4
			if i[0]=='>':
					cattiva=False
					uniprotid=i.split()[0][1:]
					regions=i.strip().split()[-1] #regions=[7-34,43-57]
					assert 'regions=' in regions
					r=regions.replace('regions=','').strip('[]').split(',')
			
					regions=[]
					for k in r:
						if k=='':
							cattiva=True
							continue
						a=k.split('-')
						
						
							
						regions+=[(int(a[0])-1,int(a[1]))]
					dizio[uniprotid]=['',[]]
			else:
				try:
					dizio[uniprotid][0]=i.strip('\n')
					a=[0]*len(i.strip('\n'))
					for k in regions:
						for p in range(k[0],k[1],1):
							a[p]=1
					dizio[uniprotid][1]=a
					if cattiva:
						del dizio[uniprotid]
				except:
					print uniprotid,"e' rotta! salto"
					salto+=1
		print 'tot prots',len(dizio.keys()),'saltate',salto
		return dizio
def main():
	#mkdssp('/home/gabriele/HD1/pasta_features/databases/top500H/',dssp_out_file='DSSP500',folder=True)
	#get_beta('DSSP500/',folder=True)
	read_rita2('../databases/rita/rita_2.txt')
	'''
	parse_dynamine(fil='dyna_pred/amyProt_backbone.pred',outfil='marshalled/dyna_back.m')
	parse_dynamine(fil='dyna_pred/amyProt_earlyFoldProb.pred',outfil='marshalled/ef.m')
	
	parse_dynamine(fil='dyna_pred/amyProt_coil.pred',outfil='marshalled/dyna_coil.m')
	parse_dynamine(fil='dyna_pred/amyProt_helix.pred',outfil='marshalled/dyna_helix.m')
	parse_dynamine(fil='dyna_pred/amyProt_sheet.pred',outfil='marshalled/dyna_sheet.m')
	parse_dynamine(fil='dyna_pred/amyProt_sidechain.pred',outfil='marshalled/dyna_side.m')
	parse_dynamine(fil='dyna_pred/amyProt_ppII.pred',outfil='marshalled/dyna_ppII.m')
	'''
if __name__ == '__main__':
	main()
