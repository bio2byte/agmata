#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  agmata.py
#  
#  Copyright 2018 Gabriele Orlando <orlando.gabriele89@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
PLOT=False
SCALE=False
from math import log
import numpy as np
import marshal,os,random,gc
#import matplotlib.pylab as plt
from sklearn.linear_model import LogisticRegression
from sources.utils import get_beta,readRegions,getScoresSVR,leggifasta
import utils as additional_utils
from scipy.stats import gaussian_kde
from sklearn.svm import SVC
from sklearn.neighbors import KernelDensity
from multiprocessing import Pool
from vector_builder.vettore_gen import build_vector
import pickle

import cPickle
class agmata():
	def __init__(self,features='dyna_back,dyna_coil,dyna_sheet,dyna_helix,dyna_side',sw=1,verbose=2):
		self.features=features
		self.sw=sw
		self.LEARNING='log_reg'
		self.verbose=verbose
		self.params=[self.LEARNING,self.features,self.sw]
	def recalculate_dssp(self,pdb_folder,dssp_out):
		print 'recalculating dssp for the PDBs contained in',pdb_folder,'. It will take A LOT (hours) for large datasets'
		additional_utils.mkdssp(pdb_folder,dssp_out_file=dssp_out,folder=True)
		print '\tdone'
	def load(self,force=False):
		params_old=pickle.load(open('marshalled/'+'model_parameters.m','r'))
		if self.params!=params_old:
			if force==False:
				print 'PARAMETERS CHANGED! NEW FITTING'
				self.fit()
			else:
				
				print 'PARAMETERS CHANGED! but force==True,keeping old params(',params_old,')'
				Dp,Da,Dn=self.strands=pickle.load(open('marshalled/discriminative.m','r'))
				self.Dp=Dp
				self.Da=Da
				self.Dn=Dn
				self.params=params_old
				self.LEARNING=params_old[0]
				self.features=params_old[1]
				self.sw=params_old[2]
		else:
			Dp,Da,Dn=self.strands=pickle.load(open('marshalled/discriminative.m','r'))
			self.Dp=Dp
			self.Da=Da
			self.Dn=Dn
	def fit(self,dssp_file='DSSP500/',):
		strands=self.calculate_strands(dssp_file=dssp_file)
		self.learn_distro(strands)
		pickle.dump(self.params,open('marshalled/'+'model_parameters.m','w'))
	def calculate_strands(self,dssp_file):
		if self.verbose>=1:
			print 'calculating strands (it will take some time)'
		strands=get_beta(dssp_file,folder=True)
		if self.verbose>=1:
			print '\tdone'
		return strands
	def learn_distro(self,strands):
		
		vet={}
		paralX=[]
		paralY=[]
		antiparalX=[]
		antiparalY=[]
		nabX=[]
		nabY=[]
		for i in strands.keys()[:]:
			vet[i]=build_vector(strands[i][0].upper(),self.features,self.sw,nomeseq=i)
			nfea=0
			for j in strands[i][1]:##paral
				paralX+=[self.compare(vet[i][j[0]],vet[i][j[1]])]
			for j in strands[i][2]:##anti
				antiparalX+=[self.compare(vet[i][j[0]],vet[i][j[1]])]	
			for j in random.sample(vet[i],20):#FFFF0):
				for k in random.sample(vet[i],20):
					nabX+=[self.compare(j,k)]
		if self.verbose>=1:
			print 'training with ',self.LEARNING,'method'
		if self.LEARNING=='kde':
			Dp = KernelDensity(bandwidth=0.33,kernel='gaussian')
			Da = KernelDensity(bandwidth=0.33,kernel='gaussian')
			Dn = KernelDensity(bandwidth=0.33,kernel='gaussian')
			Dp.fit(np.array(paralX))
			Da.fit(np.array(antiparalX))
			Dn.fit(np.array(nabX))
		elif self.LEARNING=='SVM':
			Dp = SVC(C=1.0, cache_size=200, class_weight='balanced', kernel='rbf', probability=True)
			Da = SVC(C=1.0, cache_size=200, class_weight='balanced', kernel='rbf', probability=True)
			Dn = None
			Dp.fit(paralX+nabX,[1]*len(paralX)+[0]*len(nabX))
			Da.fit(antiparalX+nabX,[1]*len(antiparalX)+[0]*len(nabX))
		elif self.LEARNING=='log_reg':
			Dp = LogisticRegression(fit_intercept=False)
			Da = LogisticRegression(fit_intercept=False)
			Dn = None
			if self.verbose>=1:
				print '# features',len(paralX[0]),'training discriminative method'
			Dp.fit(paralX+nabX,[1]*len(paralX)+[0]*len(nabX))
			Da.fit(antiparalX+nabX,[1]*len(antiparalX)+[0]*len(nabX))
			if self.verbose>=1:
				print '\tdone'
		self.Dp=Dp
		self.Da=Da
		self.Dn=Dn
		if self.verbose>=1:
			print 'saving the model'
		pickle.dump((Dp,Da,Dn),open('marshalled/discriminative.m','w'))
	def compare(self,v1,v2,COMP='vicini'):
		vf=[]
		#print v1,v2
		if COMP=='diff':
			
			if '-' in v1:
				vf=[]
				for i in v2:
					if type(i)!=str:
						vf+=[i]
				return vf
			elif '-' in v2:
				vf=[]
				for i in v1:
					if type(i)!=str:
						vf+=[i]
				return vf
			else:
			
				for i in range(len(v1)):
					
					if type(v1[i])!=str:
						vf+=[abs(v1[i]-v2[i])]
					
					else:
						if blosum.has_key((v1[i].upper(),v2[i].upper())):
							
							vf+=[blosum[(v1[i].upper(),v2[i].upper())]]
						else:
							
							vf+=[blosum[(v2[i].upper(),v1[i].upper())]]
		if COMP=='vicini':
			if '-' in v1:
				vf=[]
				for i in v2:
					if type(i)!=str:
						vf+=[i]
				return vf
			elif '-' in v2:
				vf=[]
				for i in v1:
					if type(i)!=str:
						vf+=[i]
				return vf
			else:
			
				for i in range(len(v1)):
					
					if type(v1[i])!=str:
						vf+=[v1[i],v2[i]]
					
					else:
						if blosum.has_key((v1[i].upper(),v2[i].upper())):
							
							vf+=[blosum[(v1[i].upper(),v2[i].upper())]]
						else:
							
							vf+=[blosum[(v2[i].upper(),v1[i].upper())]]
		return vf
	def predict(self,seq):
		vet=build_vector(seq,self.features,self.sw)
		x=[]
		for i in range(len(vet)):
			for j in range(i,len(vet)):
				x+=[self.compare(vet[i],vet[j])]
		
		if self.verbose>=1:
			print '\tnumero coefficienti:',2*len(x)

		if self.LEARNING=='kde':
			par=self.Dp.score_samples(x)
			ant=self.Da.score_samples(x)
			non=self.Dn.score_samples(x)
			cont=0
			f=open('coef.tmp','w')
			for i in range(len(vet)):
				for j in range(i+1,len(vet)):
					pa=-(par[cont]-non[cont])
					an=-(ant[cont]-non[cont])
					f.write(str(i+1)+' '+str(j+1)+' '+str(pa)+' '+str(an)+' 0.00000 0.00000 \n')
					cont+=1
			f.close()
		elif self.LEARNING=='log_reg' or self.LEARNING=='SVM':
			ant=self.Da.predict_log_proba(x)
			par=self.Dp.predict_log_proba(x)
			cont=0
			f=open('coef.tmp','w')
			for i in range(len(vet)):
				for j in range(i,len(vet)):
					pa=-(par[cont][1]-par[cont][0])
					an=-(ant[cont][1]-ant[cont][0])
					f.write(str(i+1)+' '+str(j+1)+' '+str(pa)+' '+str(an)+' 0.00000 0.00000 \n')
					cont+=1
			f.close()

		else:
			raise('unknown discriminative method')
		f=open('seq.tmp','w')
		f.write(seq+'\n')
		f.close()

		os.system('./bin/agmata_c_final seq.tmp 600 0')
		f=open('aggr_profile.dat','r')
		output=[]
		for i in f.readlines():
			output+=[float(i)]
		gc.collect()
		f.close()
		os.system('rm aggr_profile.dat nseq pairing_mat.dat best_pairings_list.dat coef.tmp seq.tmp')
		if SCALE:
			scaler=pickle.load(open('scaler.m','r'))
			output=scaler.transform(np.array(output).reshape(-1,1)).reshape((-1))
		if PLOT:
			
			import matplotlib.pylab as plt
			
			#plt.axis('off')
			#plt.xticks([])
			#plt.yticks([])
			plt.xlabel('position')
			plt.ylabel('aggregation propensity')
			plt.plot(range(len(output)),output,'b')
			plt.show()
			plt.savefig('plots/'+'protname'+'.png',dpi=350)
			plt.clf()
		return output
if __name__ == '__main__':
    import sys
    run_ataxin()
    ghf
    sys.exit(main(sys.argv))
